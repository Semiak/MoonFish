#!/bin/sh

# This file contains all your moonfish instance info.
# Do only modify the needed variables.

instance_name = "[INSTANCE NAME HERE]" #your moonfish instance name. Can be something like tony_stark or thanos999.
owner_name = "[YOUR_NAME]" #once presented for the contest of the best explanatory variable name. Didn't win ;(
owner_github = "[GITHUB_USERNAME]" # same below ^

package_installation = {
    database = { #database or FTP information
        "url": "https://ftp.myprogramurl.com" #your FTP url (moonfish will try to download the packages from here)
        "port": "None" #your database port (if you dont have any leave it in None) (moonfish will ask this port for packages)
        "sub-directory": "/apps" #subdomain moonfish will look for programs (e.g. "https://fpt.mywebsite.com/apps"). If you don't have any, leave it in blank ("/")
        "recipe-extension": ".mnfs" #please leave DO NOT change this variable unless you know what you are doing
    }
    prohibited_packages = ["main.mnfs"] #simple array of files you dont want moonfish allow your users to download.
    ignored_extensions = [".mnfsx"] #same as prohibited_packages but instead of files, extensions.
}

give_credits = True #please consider giving us credits. ;)
